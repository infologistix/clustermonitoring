# Helm Values

Hier sind sämtliche Helm Values für eine Beispiel Installation auf der THF TRU. 


## Usage

Jedes Helm Chart kann daraus installiert werden. Dazu einfach folgende Befehlskette verwenden:

```bash
helm install <release> ./<chart> -f ./values/<chart>.yaml
```
