#!/bin/bash

echo "Scale Prometheus down"
kubectl patch prometheus $(kubectl get prometheus -oname -n cattle-monitoring-system | awk -F'/' '{print $2}') -n cattle-monitoring-system --type 'json' -p '[{"op":"replace", "path": "/spec/replicas", "value": 1}]'
PROMETHEUS=$(kubectl get sts -oname | grep prometheus)

while [ $(kubectl get $PROMEHTEUS -ojson | jq -r '.status.replicas') != 0 ]
do
sleep 5
echo "Waiting for Prometheus to terminate..."
done

echo "Get PVC from Prometheus"
PVC_NAME=$(kubectl get $PROMETHEUS -ojson | jq -r '.spec.volumeClaimTemplates[0].metadata.name')
PVC=$(kubectl get pvc -oname | grep $PVC_NAME | awk -F'/' '{print $2}')
PV=$(kubectl get pvc $PVC -ojson | jq -r '.spec.volumeName')
CAPACITY=$(kubectl get pv $PV -ojson | jq -r '.spec.capacity.storage')

echo "Set PV to Retain"
kubectl patch pv $PV -n cattle-monitoring-system -p '{"spec":{"persistentVolumeReclaimPolicy":"Retain"}}'

echo "Delete PVC from rancher Monitoring"
kubectl delete pvc $PVC

echo "Free up PV to be newly bound"
kubectl patch pv $PV -n cattle-monitoring-system -ü '{"spec":{"claimRef": {}}}'

echo "Creating new PVC for new Prometheus"
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  labels:
    app.kubernetes.io/instance: kube-prometheus-stack-prometheus
    app.kubernetes.io/managed-by: prometheus-operator
    app.kubernetes.io/name: prometheus
    operator.prometheus.io/name: kube-prometheus-stack-prometheus
    operator.prometheus.io/shard: "0"
    prometheus: kube-prometheus-stack-prometheus
  name: prometheus-kube-prometheus-stack-prometheus-db-prometheus-kube-prometheus-stack-prometheus-0
  namespace: cattle-monitoring-system
spec:
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: $CAPACITY
  storageClassName: portworx
  volumeMode: Filesystem
  volumeName: $PV
EOF

echo "Done. Proceed with Installing the new Kube-Prometheus Stack after removing old Rancher-Monitoring"