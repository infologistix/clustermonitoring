#!/bin/bash

echo -e "convert monitoring crds..."
releaseName=${1:-'monitoring-crd'}

echo "Patching Release:"  $releaseName

for ressource in $(kubectl get crd -oname | grep "monitoring.coreos"); do
kubectl label $ressource --overwrite "app.kubernetes.io/managed-by"="Helm"
kubectl annotate $ressource --overwrite "meta.helm.sh/release-name"=$releaseName "meta.helm.sh/release-namespace"="cattle-monitoring-system"
done

echo "Done..."
echo "Suspend Helm Release"
flux suspend helmrelease rancher-monitoring-crd -n cattle-monitoring-system
flux suspend helmrelease rancher-monitoring -n cattle-monitoring-system
echo "remove Helm installation from cluster"
helm uninstall rancher-monitoring-crd --no-hooks
helm uninstall rancher-monitoring
echo "Removing legacy kubelet Service"
kubectl delete svc rancher-monitoring-kubelet -n kube-system 
kubectl delete hr rancher-monitoring rancher-monitoring-crd
echo "done"
echo "You may proceed installing new Monitoring Stack. Begin with new CRDs..."
