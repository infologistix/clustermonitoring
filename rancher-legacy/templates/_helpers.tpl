{{/*
Expand the name of the chart.
*/}}
{{- define "rancher-legacy.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "rancher-legacy.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "rancher-legacy.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "rancher-legacy.labels" -}}
helm.sh/chart: {{ include "rancher-legacy.chart" . }}
{{ include "rancher-legacy.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "rancher-legacy.selectorLabels" -}}
app.kubernetes.io/name: {{ include "rancher-legacy.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "rancher-legacy.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "rancher-legacy.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Lookup cluster info from configMap
*/}}
{{- define "cluster-info" -}}
{{- $configMap := lookup "v1" "ConfigMap" "cluster-info" "cluster-info" -}}
{{- if $configMap -}}
{{- if (hasKey $configMap.data .) -}}
{{- index $configMap.data . -}}
{{- else -}}
{{- fail "Key not found in ConfigMap 'cluster-info'" -}}
{{- end -}}
{{- else -}}
{{- print . -}}
{{- end -}}
{{- end -}}

{{/*
*/}}
{{- define "oauth2.domain" -}}
{{- $domain := ""}}
{{- if eq (.Values.global.grafana.auth).domain "idst" -}}
{{- $domain = "i" -}}
{{- else if eq (.Values.global.grafana.auth).domain "sdst" -}}
{{- $domain = "s" -}}
{{- else -}}
{{- $domain = "" -}}
{{- end -}}
{{- printf "*d1@%sdst.%sbaintern.de" $domain $domain -}}
{{- end -}}

{{/*
*/}}
{{- define "oauth2.endpoint" -}}
{{- $domain := ""}}
{{- if eq (.Values.global.grafana.auth).domain "idst" -}}
{{- $domain = "-i" -}}
{{- else if eq (.Values.global.grafana.auth).domain "sdst" -}}
{{- $domain = "-s" -}}
{{- else -}}
{{- $domain = "" -}}
{{- end -}}
{{- printf "https://sts%s.arbeitsagentur.de/adfs" $domain -}}
{{- end -}}
