# Helm Chart Repository für das Cluster Monitoring

Enthält sämtliche Helm Charts des Cluster Monitorings.

Einzelne Helm Charts werden direkt heir aus dem Repo heraus gebaut. Änderungen an Dateien können daher jederzeit zu einem neuen Helm Chart führen!

## Charts

- monitoring-crd
- prometheus-node-exporter
- prometheus-adapter
- pushproxies
- kube-state-metrics
- grafana
- kube-prometheus-stack
- rancher-legacy (braucht vorab Istio)

### monitoring-crd

Dependencies:

- Namespace

Das Chart monitoring-crd installiert die entsprechend benötigten CRDs des Monitorings und kann diese auch entsprechend updaten. Dies ist wichtig, da Helm eigentlich kein CRD Update unterstützt.

### prometheus-node-exporter

Dependencies:

- monitoring-crd

Prometheus Exporter für Node Metriken

### prometheus-adapter

Dependencies:

- kube-prometheus-stack

Stellt dem Api-Server die entsprechenden Prometheus Metriken zur Verfügung (z.B. Horizontal Pod Autoscaler)

### pushproxies

Dependencies:

- monitoring-crd

Werden benötigt, wenn ein hardened Cluster eingerichtet ist. Push Metriken eines Nodes in den Prometheus (kubelet, api-server, etcd, ...)

### kube-state-metrics

Dependencies:

- monitoring-crd

Kube-State Metrics Agent

### grafana

Dependencies:

- kube-state-metrics
- rancher-istio (istio-system)

Die Grafana Installation. Installiert lediglich Grafana, mehr nicht :D

### kube-prometheus-stack

Dependencies:

- grafana

Installiert den Prometheus Operator und Alertmanager und Cluster Prometheus

### rancher-legacy

Dependencies:

- rancher-istio (istio-system)
- grafana

## Usage

In der oben angegeben Reihenfolge sind die entsprechenden Charts zu installieren. Das heißt: jedes Chart ist von den vorherigen mindestens zu einem abhängig!

Für die Installation ist ein namespace `cattle-monitoring-system` anzulegen. In diesen Namespace muss dann entsprechend das Helm Chart installiert werden.

```bash
helm install <release> ./<chart> -f ./values/<chart>.yaml

# templating
helm template grafana ./grafana -f ./values/grafana.yaml
```

### fleet

TBD
